# discussion visualiser of controversial topics project

## Main usage

For a controversial question like "Should weed be legalized?" or "should the war in UA be funded by third party states?" the website should display an objective, unopinionated overview of all pro and counterarguments and their relations.

### Visualisations could be for example:

- [a force directed graph](https://user-images.githubusercontent.com/57335825/81265424-a8747800-9075-11ea-88fe-3f7a21de77d0.png) where the question is central and counterarguments are red bubbles and pro are blue. Could act as an overview about the whole discussion.
- [a pro contra list](https://cdn.vertex42.com/ExcelTemplates/Images/pros-and-cons-list-template.png) that shows pro and contra arguments on each side of a table and some links in between. Probably expandable with an [accordeon](https://mui.com/material-ui/react-accordion/).
- A detail view of each argument.

### Features could be

- deep links to an argument so it can be shared on social media instead of typing it out
- further sources on each argument so readers can research themselves

## Motivation

- tool can be used as a condensed overview of pro and contra points of a topic without spending the time of researching it
- tool can be used in the future to base decisions on for human actors. (Assuming it's possible to compute some value to each argument)

## Useful resources

### Scraping articles for a topic

- GET https://newsapi.org/v2/everything?language=en&q=ukraine
- https://newsapi.org/docs/guides/how-to-get-the-full-content-for-a-news-article

### Embedding

- https://github.com/openai/openai-cookbook/blob/5783656852d507c335955d14875ebc9902f628ef/examples/Embedding_long_inputs.ipynb
- https://github.com/openai/openai-cookbook/blob/5783656852d507c335955d14875ebc9902f628ef/examples/Question_answering_using_embeddings.ipynb

## Schemas

- stakeholder (e.g. The US, the american people, etc..)
  - name
  - link
- statement
  - id
  - stakeholder (the entity that made this statement)
  - about (the entity they are talking about)
  - content (the statement)
  - source (a link to the article where the statement is from)
  - timestamp (when the statement was made/date of article)
  - embedding (generated by openai)
- argument
  - id (a unique id so this argument can be referenced by others)
  - question (a question that questions this argument. So if the argument is `bananas are blue` then the question should ask `are bananas blue`. This is probably necessary because the database will be filled with false information.)
  - isPro ("is this argument pro or anti towards the parents?")
  - parents (argument ids that this argument responds to)
  - statementIds (statements that were used to make this argument)
  - stakeHolders (Could be interesting to visualise how this argument would affect a certain stakeholder. But probably hard to tell.)

## Functionality / Algorithm

### Step 1: Create database of statements (continuosly updated)

- get articles manually or automatically (newsapi.org) and put them into an s3 Bucket txt document
- for each article ask chatgbt to extract statements in the format of the `statement` table above.
  - The prompt should be something like: `Find objective statements that are not the authors own opinion but a statement by another person mentioned in this article.`
- save each statement to the db

### Step 2: generate web of arguments based on the database

1. Write the question to chatgbt and get the embedding
1. based on the embedding query the database and find the relevant statements
1. extract only the id and the statement and post them line by line in a text document without json formatting or other info (the other information can be queried later by the id)
1. send the text document together with the question and a prompt to chatgbt and ask it to respond with an array of the `argument` schema
1. the prompt should ask for only one level deep of arguments.

- Iterate over all arguments returned by chatgbt and for each of them repeat from step 1 with the argument's `question` (a question that questions the statement) until there is only pro or only contra arguments.

## Challenges

Especially for controversial topics there is alot of opinions and few facts in articles. The main challenge is quality of data and validity. It needs to be assured in general that the media sources are balanced in their reporting or the pool of media sources is balanced out as a whole with many differing statements.

- **the author's bias changes data**: for example in war each side will inflate the number of fatalities on the other side. Authors that are affiliated with any side will round up/down numbers. -> Should be fine if data of both "sides" is in the database. chatgbt will make an average or mention that it is disputed or split it up in a seperate counterargument.
- **the author's bias hides or overstates certain information**: This could also be avoided by diversity of authors
